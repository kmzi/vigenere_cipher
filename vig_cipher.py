class RussianAlphabet:
    def __init__(self):
        self.letters = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя .,?!')
        # self.letters = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')
        self.m = len(self.letters)
        # for i, j in enumerate(self.letters):
        #     print(f'{i}: {j}')


class Vigenere:
    def __init__(self, alph=RussianAlphabet()):
        self.alph = alph.letters
        self.m = alph.m

    def get_key(self, key, text):
        text_len = len(text)
        # new_key = list(key)
        new_key = []
        for letter in key:
            new_key.append(self.alph.index(letter))
        # print(new_key)
        ind = 0
        while len(new_key) < text_len:
            new_key.append(new_key[ind])  # % key_len])
            ind += 1
        # print('new key: ', new_key)
        return new_key

    def get_text(self, text):
        new_text = []
        for letter in text:
            new_text.append(self.alph.index(letter))
        return new_text

    def encrypt(self, key, text):
        # c = (p + k) mod m
        key = self.get_key(key.lower(), text)
        text = self.get_text(text.lower())
        encrypted_text = []
        for p, k in zip(text, key):
            encrypted_text.append(self.alph[(p + k) % self.m])
        return ''.join(encrypted_text)

    def decrypt(self, key, text):
        # p = (c + m - k) mod m
        key = self.get_key(key.lower(), text)
        text = self.get_text(text.lower())
        decrypted_text = []
        for c, k in zip(text, key):
            decrypted_text.append(self.alph[(c + self.m - k) % self.m])
        return ''.join(decrypted_text)

    # шифровка и расшифровка с самоключом:
    def self_encrypt(self, key, text):
        '''
        В случае самоключа ключом и является единственная буква. Гамма последовательность же формируется либо путём
        добавления к этой букве открытого текста, либо последовательно получаемого шифртекста.
        '''
        return self.encrypt(key + text, text)   # к ключу добавляется исходный текст и текст шифруется с получившимся ключом

    def self_decrypt(self, key, text):
        # key = self.get_key(key + text, text)
        key = [self.alph.index(key)]
        text = self.get_text(text)
        decrypted_text = []
        key_ind = 0
        while text:     # к ключу постепенно добавляется расшифрованный текст
            letter = text.pop(0)
            decrypted_letter_number = (letter + self.m - key[key_ind]) % self.m
            key.append(decrypted_letter_number)
            key_ind += 1
            decrypted_text.append(self.alph[decrypted_letter_number])
        return ''.join(decrypted_text)


def main():
    while True:
        try:
            cipher = Vigenere()
            method = int((input('Выберете метод шифрования: шифр Виженера (1) или шифр Виженера с самоключом (2): ')))

            if method == 1:
                encrypt_function = cipher.encrypt
                decrypt_function = cipher.decrypt
            elif method == 2:
                encrypt_function = cipher.self_encrypt
                decrypt_function = cipher.self_decrypt
            else:
                print('Выбран неверный метод. Попробуйте снова.')
                continue

            option = int((input('Выберете опцию: зашифровать (1) или расшифровать (2) текст: ')))
            if option not in [1, 2]:
                print('Неверная опция. Попробуйте снова.')
                continue

            key = input('Введите ключ: ')

            text = input('Введите текст: ')
            if option == 1:
                print('Зашифрованный текст: ', encrypt_function(key, text))
            elif option == 2:
                print('Расшифрованный текст: ', decrypt_function(key, text))

        except ValueError:
            print('Введены неправильные данные. Попробуйте снова.')  # qw or sth went wrong


if __name__ == '__main__':
    main()
